mon ami les plaisirs auxquels on nest pas habitué gênent plus que les chagrins dont on avait lhabitude
