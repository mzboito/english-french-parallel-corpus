ces pelouses à tissu serré douces au pied eussent rivalisé avec les plus moelleux tapis tissés par la main des hommes
