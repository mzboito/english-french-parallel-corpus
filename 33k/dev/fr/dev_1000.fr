la perspective de cette tâche me remettait en mémoire comme dans un rêve les événements de la veille et cette pensée seule était pour moi toute la réalité de la vie
