la pâleur du moine devint de la lividité et il sourit dune si étrange façon que raoul qui ne le quittait pas des yeux sentit ce sourire lui serrer le coeur comme une insulte
