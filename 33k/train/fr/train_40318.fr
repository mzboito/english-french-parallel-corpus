voilà donc une action qui était accomplie non par devoir ni par inclination immédiate mais seulement dans une intention intéressée
