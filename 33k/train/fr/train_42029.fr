(un moraliste n’est il pas l’antithèse d’un puritain quand bien entendu ce penseur est un moraliste qui regarde la morale comme une chose douteuse énigmatique bref comme un problème
