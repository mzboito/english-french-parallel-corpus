la haine est aveugle la colère étourdie et celui qui se verse la vengeance risque de boire un breuvage amer
