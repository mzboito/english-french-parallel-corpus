comme si le destin trahissait le soldat dans la mort il exposait à ses ennemis cette pauvreté que vivant il eut peut être caché à ses amis mêmes
