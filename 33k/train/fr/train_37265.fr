la seule occasion dans laquelle julien semble s’être écarté de sa clémence ordinaire est l’exécution d’un jeune imprudent qui d’une main faible et impuissante voulut saisir les rênes de l’empire
