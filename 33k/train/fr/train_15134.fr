jétais surpris de constater que ce qui auparavant nétait que désert et tristesse se parait à présent de fleurs et de verdure
