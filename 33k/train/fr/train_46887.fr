un guerrier s’approcha de moi transportant des armes des ornements des insignes et tout un attirail exactement identiques à ceux qu’il portait lui même
