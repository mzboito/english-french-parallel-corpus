non seulement mon amour et ma dignité ne me permettaient pas d’agir ainsi mais encore j’étais bien convaincu qu’au point où elle en était arrivée marguerite mourrait plutôt que d’accepter ce partage
