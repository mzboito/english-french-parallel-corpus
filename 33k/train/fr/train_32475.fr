elle devient alors indépendante de la chambre de thé et ne connaît plus d’autre gloire que celle que lui impose le vase choisi
