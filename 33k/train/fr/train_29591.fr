la pitié fait presque un effet risible chez l’homme qui cherche la connaissance de même que les mains fines chez le cyclope
