lhomme nest homme que par la société laquelle de son côté ne se soutient que par léquilibre et lharmonie des forces qui la composent
