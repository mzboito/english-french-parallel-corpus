elle avait les yeux rougis et gonflés et il semblait que même alors ses larmes ne fussent retenues qu’avec difficulté
