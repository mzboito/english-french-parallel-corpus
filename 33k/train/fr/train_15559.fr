mes esprits sélevaient devant le visage enchanteur de la nature le passé seffaçait de ma mémoire le présent était tranquille et lavenir sannonçait riche despoir et de joie
