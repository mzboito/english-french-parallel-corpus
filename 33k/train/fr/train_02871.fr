ce jour là villefort le vit poindre blafard et sinistre et sa lueur bleuâtre vint faire reluire sur le papier les lignes tracées à lencre rouge
