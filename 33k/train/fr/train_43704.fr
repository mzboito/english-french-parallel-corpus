comment peut il être noble de désirer rendre sa vie infinie et mesquin de désirer la rendre immortelle
