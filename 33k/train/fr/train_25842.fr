la nature attache une sanction sévère à de tels crimes et la loi humaine a le devoir de renforcer les décrets de la nature
