il y avait dans sa pose cette rigidité que simulent les acteurs mais il savait trop que chez sa femme il ne sagissait pas de simulation
