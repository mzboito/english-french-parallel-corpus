dans de certaines situations violentes les instincts se satisfont comme bon leur semble sans que la pensée s’en mêle
