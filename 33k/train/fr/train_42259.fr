ny a t il pas dans la pensée humaine des ailes qui frémissent et des cordes sonores qui se tendent
