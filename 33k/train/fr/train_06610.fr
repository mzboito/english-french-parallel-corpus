dans ces moments là quels regards douloureux il tournait vers le cloître ce sommet chaste ce lieu des anges cet inaccessible glacier de la vertu
