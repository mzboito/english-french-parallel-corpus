quand la conversation venait à tomber sur la politique campbell observait un silence et exprimait une modération qui pouvaient être commandés par la prudence
