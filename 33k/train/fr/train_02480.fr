son tempérament volontaire toujours disposé à exiger sans jamais vouloir se plier à aucune concession écartait de lui tout mouvement amical toute démonstration sympathique
