en présence de ce nouveau et imminent danger que devenaient nos chances de salut et comment empêcher la solidification de ce milieu liquide qui eût fait éclater comme du verre les parois du nautilus
