du cœur de la cité assoupie qui devait bientôt se remplir de mille passions aucun son ne s’élevait les flots de la vie ne circulaient pas encore le sommeil les recouvrait de sa glace
