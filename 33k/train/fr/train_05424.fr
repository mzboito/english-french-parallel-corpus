dans ce temps à la fois bourgeois et héroïque en présence des idées qui avaient leurs chevaliers les intérêts avaient leurs paladins
