la répression a autant de régiments que la barricade a dhommes et autant darsenaux que la barricade a de cartouchières
