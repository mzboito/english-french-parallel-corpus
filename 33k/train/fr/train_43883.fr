et cependant il avait une conscience très profonde de sa supériorité et il considérait comme lui étant dus tous les hommages qu’il recevait et l’absence de ces hommages le blessait comme un affront
