marianne se reprocha sévèrement ce qu’elle avait dit mais son propre pardon aurait pu être plus rapide si elle avait su à quel point ses paroles avaient peu contrarié sa sœur
