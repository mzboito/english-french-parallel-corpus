une dispersion d’existences mystérieuses s’amalgame à notre vie par ce bord de la mort qui est le sommeil
