comme les musiciens qui essayaient vainement de faire vibrer la harpe de lungmen ils ne chantent qu’eux mêmes
