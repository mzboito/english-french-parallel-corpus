les grandes époques de notre vie sont celles où nous avons le courage de considérer ce qui est mauvais en nous comme ce que nous avons de meilleur
