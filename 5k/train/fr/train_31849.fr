celui qui ne sait pas trouver le chemin qui conduit à son idéal vit de façon plus frivole plus insolente que l’être sans idéal
