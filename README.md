# english-french-parallel-corpus

Filtered version of the LibreSpeech+French corpus from Kocabiyikoglu et al. 2018. Sentences longer than 100 tokens, alignments with punctuation and empty lines were removed. 


#### Original Paper: *Augmenting Librispeech with French Translations: A Multimodal Corpus for Direct Speech Translation Evaluation*

@article{kocabiyikoglu2018augmenting,
  title={Augmenting Librispeech with French translations: A multimodal corpus for direct speech translation evaluation},
  author={Kocabiyikoglu, Ali Can and Besacier, Laurent and Kraif, Olivier},
  journal={arXiv preprint arXiv:1802.03142},
  year={2018}
}
